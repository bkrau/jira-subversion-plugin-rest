package com.atlassian.jira.plugin.ext.subversion.rest;

import com.atlassian.jira.issue.index.IndexException;
import com.atlassian.jira.plugin.ext.subversion.MultipleSubversionRepositoryManager;
import com.atlassian.jira.plugin.ext.subversion.revisions.RevisionIndexer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/")
public class UpdateRepoRestResource {

    Logger log = LoggerFactory.getLogger(UpdateRepoRestResource.class);
    private final RevisionIndexer indexer;

    public UpdateRepoRestResource(MultipleSubversionRepositoryManager manager) {
        this.indexer = manager.getRevisionIndexer();
    }

    @PUT
    @Path("/commits")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response getUncompletedUsers(final UpdateRepoRestResourceModel model) {
        String repositoryUrl = model.getRepositoryUrl();
        String token = model.getToken();
        try {
            boolean found = indexer.updateIndexForRepoByUrl(repositoryUrl, token);
            if (!found) {
                return Response.
                        status(Response.Status.NOT_FOUND).
                        entity("\"Could not update svn repository: repository not found or wrong token").
                        build();
            }
        } catch (IndexException e) {
            log.error("Could not update svn repository: ", e);

            return Response.serverError().entity(e.getMessage()).build();
        }

        return Response.ok("Updated index for repo: " + repositoryUrl).build();
    }
}
